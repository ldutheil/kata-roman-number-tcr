package com.starter.app;

import java.util.Collections;

class RomanConverter {
    public static final String ROMAN_ZERO = "";
    public static final String ROMAN_ONE = "I";
    public static final String ROMAN_FIVE = "V";
    public static final String ROMAN_TEN = "X";

    private static final int FIVE = 5;

    static String convert(int number) {
        int moduloFive = number % FIVE;
        int dividedByFive = number / FIVE;

        if (dividedByFive == 0) {
            return compute(moduloFive, ROMAN_ZERO, ROMAN_FIVE);
        }
        if (dividedByFive == 1) {
            return compute(moduloFive, ROMAN_FIVE, ROMAN_TEN);
        }
        return ROMAN_TEN;
    }

    private static String compute(int moduloFive, String romanZero, String romanFive) {
        if (moduloFive < FIVE - 1) {
            return romanZero + repeat(moduloFive, ROMAN_ONE);
        }
        return ROMAN_ONE + romanFive;
    }

    private static String repeat(int number, String letter) {
        return String.join("", Collections.nCopies(number, letter));
    }
}
