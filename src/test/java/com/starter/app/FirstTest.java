package com.starter.app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FirstTest {

    @Test
    void should_return_I() {
        assertEquals("I", RomanConverter.convert(1));
    }

    @Test
    void should_return_II() {
        assertEquals("II", RomanConverter.convert(2));
    }

    @Test
    void should_return_V() {
        assertEquals("V", RomanConverter.convert(5));
    }

    @Test
    void should_return_X() {
        assertEquals("X", RomanConverter.convert(10));
    }

    @Test
    void should_return_IV() {
        assertEquals("IV", RomanConverter.convert(4));
    }

    @Test
    void should_return_VI() {
        assertEquals("VI", RomanConverter.convert(6));
    }

    @Test
    void should_return_IX() {
        assertEquals("IX", RomanConverter.convert(9));
    }

}
